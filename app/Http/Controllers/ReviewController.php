<?php

namespace App\Http\Controllers;
use \App\Review;
use \Illuminate\Support\Facades\Storage;
use \Intervention\Image\Facades\Image;

use Illuminate\Http\Request;

class ReviewController extends Controller
{
    private $request;
    private $errors;
    
    public function index(Request $request)
    {
        return view('templates.index');
    }
    
    public function all(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        
        $this->request = $request;
        
        $limit = $this->getParams('limit', 50);
        $page = $this->getParams('page', 1);
        $order = $this->getParams('order', 'created_at');
        $sort = $this->getParams('sort', 'desc');
        
        $result = Review::orderBy($order, $sort)
            ->limit($limit)
            ->offset($page * $limit - $limit)->get()->toArray();
        
        foreach ($result as $index=>$item)
        {
            if (strlen($item['files']) > 0)
            {
                $result[$index]['files'] = unserialize($item['files']);
            }
        }
        
        $pages = ceil(Review::count() / $limit);
        
        return ['result' => $result, 'pages' => $pages];
    }
    
    public function valid(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        
        $this->request = $request;
        
        if ($this->getValid())
            return ['errors' => $this->getValidError()];
                
        return ['result' => 'OK'];
    }
    
    public function add(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        
        $this->request = $request;
        
        if ($this->getValid())
            return ['errors' => $this->getValidError()];
        
        $id = Review::add([
            'name'  => $this->getParams('name'),
            'email' => $this->getParams('email'),
            'body'  => $this->getParams('body'),
            'files' => serialize($this->getPathFiles()),
        ]);
        
        if (intval($id) <= 0)
            return ['errors' => ['id' => 'not id']];
        
        return ['result' => ['id' => $id]];
    }
    
    protected function getParams($name, $default=null) 
    {
        return !is_null($this->request->input($name)) ? $this->request->input($name) : $default;
    }
    
    protected function getValid()
    {
        $values = [
            'name' => $this->getParams('name'),
            'email' => $this->getParams('email'),
            'body' => $this->getParams('body'),
        ];
        
        $this->errors = Review::fieldValid($values);
        
        return is_array($this->errors) || count($this->errors) <= 0;
    }
    
    protected function getValidError()
    {
        if (is_array($this->errors) || count($this->errors) <= 0)
            return $this->errors;
        
        return false;
    }
    
    protected function getPathFiles()
    {
        $arPath = array();
        
        if ($this->request->hasFile('image')) 
        {
            
            foreach ($this->request->file('image') as $file)
            {
                $path = $file->store('uploads', 'public');
                
                $filepath = storage_path('app/public/' . $path); 
                
                $img = Image::make($filepath)->resize(320, 240, function ($constraint) {
                    $constraint->aspectRatio();
                });
                
                $img->save($filepath);
                
                $arPath[] = Storage::url($path);
            }
        }
        
        return $arPath;
    }
}

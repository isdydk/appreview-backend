<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Review extends Model
{
    protected $fillable = ['name', 'email', 'files', 'body'];
    
    public static function getList ($arParams = array())
    {
        $arResult = array();
        
        $result = (new self())->getTable();
        
        return $result;
    }
    
    public static function fieldValid($values) 
    {
        $validator = Validator::make($values, [
            'name'  => 'required|max:50',
            'email' => 'required|email',
            'body' => 'required',
        ]);
        
        if ($validator->fails())
            return $validator->errors();
        
        return true;
    }
    
    public static function add($values) 
    {
        return Review::insertGetId(
            array_merge(
                $values, 
                array(
                    'created_at' => \DB::raw('CURRENT_TIMESTAMP'), 
                    'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
                )
            )
        );
    }
    
}

<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Удалим имеющиеся в таблице данные
        Review::truncate();

        $faker = \Faker\Factory::create();

        // А теперь давайте создадим 50 статей в нашей таблице
        for ($i = 0; $i < 50; $i++) {
           Review::create([
                'title' => $faker->sentence,
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'files' => '',
                'body' => $faker->paragraph,
            ]);
        }
    }
}

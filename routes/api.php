<?php

use Illuminate\Http\Request;
Use App\Review;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('review', 'ReviewController@all');
Route::post('review/valid', 'ReviewController@valid');
Route::post('review/add', 'ReviewController@add');

 